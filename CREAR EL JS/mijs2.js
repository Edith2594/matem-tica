var SCREEN_WIDTH = 800;
	var SCREEN_HEIGHT = 600;

	var canvas;
	var context;

	init();

	function init() {

		canvas = document.getElementById( 'micanvas' );
		
		if (canvas && canvas.getContext) {
			context = canvas.getContext('2d');
			
			windowResizeHandler();
			
		}
	}

	
	function windowResizeHandler() {
		//SCREEN_WIDTH = window.innerWidth;
		//SCREEN_HEIGHT = window.innerHeight;
		
		canvas.width = SCREEN_WIDTH;
		canvas.height = SCREEN_HEIGHT;
		
		canvas.style.position = 'absolute';
		canvas.style.left = (window.innerWidth - SCREEN_WIDTH) * .5 + 'px';
		canvas.style.top = (window.innerHeight - SCREEN_HEIGHT) * .5 + 'px';
	}

